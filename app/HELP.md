¿Qué debe tener un ejercicio?
Un id único
Su nombre
El usuario
Músculos primarios
Músculos secundarios (opcional)
Histórico de sesiones (últimas 10 sesiones)

¿Qué debe tener una rutina?
Un id único
Un usuario
Un nombre
El documento donde se encuentra la rutina

¿Qué debe tener una sesión de entreno?
Un id único
Una fecha (día, hora, mes y año)
El usuario
El número de series por ejercicio
Por cada serie, el número de repeticiones y peso utilizado (en lbs y kgs)
Nota: Debe poder editarse la rutina para cada sesión de entreno

Por hacer:
Mi perfil