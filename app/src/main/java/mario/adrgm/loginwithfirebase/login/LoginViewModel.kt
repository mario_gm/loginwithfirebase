package mario.adrgm.loginwithfirebase.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.launch
import mario.adrgm.loginwithfirebase.utils.Constants
import mario.adrgm.loginwithfirebase.utils.SharedPreferencesApp

class LoginViewModel: ViewModel() {
    var connection = MutableLiveData<Boolean>()

    fun setConnectionVariable(isConnected: Boolean) {
        viewModelScope.launch {
            connection.postValue(isConnected)
        }
    }

    fun logOut(sharedPreferencesApp: SharedPreferencesApp) {
        sharedPreferencesApp.deleteData(Constants.EMAIL)
        Firebase.auth.signOut()
    }
}