package mario.adrgm.loginwithfirebase.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns.*
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import mario.adrgm.loginwithfirebase.R
import mario.adrgm.loginwithfirebase.account.OptionsActivity
import mario.adrgm.loginwithfirebase.databinding.ActivityLoginBinding
import mario.adrgm.loginwithfirebase.utils.Constants
import mario.adrgm.loginwithfirebase.utils.SharedPreferencesApp

class LoginActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var loginBinding: ActivityLoginBinding
    private val TAG = "Login_info"
    private lateinit var sharedPreferencesApp: SharedPreferencesApp

    enum class LoginType() {
        ACTIVE,
        SIGN_IN,
        REGISTER;
    }

    companion object {
        const val USER = "user"
        var currentUser: FirebaseUser? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginBinding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(loginBinding.root)
        sharedPreferencesApp = SharedPreferencesApp(this)
        auth = Firebase.auth
    }

    override fun onStart() {
        super.onStart()
        currentUser = auth.currentUser
        if (currentUser != null && sharedPreferencesApp.getStringData(Constants.EMAIL) != null){
            updateUI(currentUser, LoginType.ACTIVE)
        }
    }

    fun createNewUser(email: String, password: String){
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "createUserWithEmail:success")
                    val user = auth.currentUser
                    updateUI(user, LoginType.REGISTER)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.d(TAG, getString(R.string.error_firebase_auth), task.exception)
                    updateUI(null, LoginType.REGISTER)
                }
            }
    }

    fun singIn(email: String, password: String){
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithEmail:success")
                    val user = auth.currentUser
                    updateUI(user, LoginType.SIGN_IN)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.d(TAG, getString(R.string.error_firebase_auth), task.exception)
                    updateUI(null, LoginType.SIGN_IN)
                }
            }
    }

    fun goToRegister(view: View){
        val action = LoginFragmentDirections.actionLoginFragmentToRegisterFragment()
        view.findNavController().navigate(action)
    }

    fun validateEmail(email: String): Boolean {
        return EMAIL_ADDRESS.matcher(email).matches()
    }

    fun validatePassword(password: String): Boolean {
        val regex = Regex("^[a-zA-Z0-9]{8,12}$")
        return regex.matches(password)
    }

    private fun updateUI(user: FirebaseUser?, type: LoginType) {
        if (user != null) {
            val intentToAccount = Intent(this, OptionsActivity::class.java)
            intentToAccount.putExtra(USER, user)
            if (sharedPreferencesApp.getStringData(Constants.EMAIL) != null) {
                user.email?.let { sharedPreferencesApp.writeStringData(Constants.EMAIL, it) }
            }
            finish()
            startActivity(intentToAccount)
        } else if (type != LoginType.ACTIVE) {
            Toast.makeText(baseContext, getString(R.string.error_firebase_auth),
                Toast.LENGTH_SHORT).show()
        }
    }
}