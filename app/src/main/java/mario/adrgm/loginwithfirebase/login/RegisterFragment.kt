package mario.adrgm.loginwithfirebase.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import mario.adrgm.loginwithfirebase.R
import mario.adrgm.loginwithfirebase.databinding.FragmentRegisterBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [RegisterFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RegisterFragment : Fragment(), View.OnClickListener {
    private var bindingForRegister: FragmentRegisterBinding? = null
    private val registerBinding get() = bindingForRegister!!
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        bindingForRegister = FragmentRegisterBinding.inflate(inflater, container, false)
        return registerBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    private fun setListeners(){
        registerBinding.createAccount.setOnClickListener(this)
    }
    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RegisterFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RegisterFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(view: View?) {
        when (view?.id){
            registerBinding.createAccount.id -> {
                goToCreateAccountWithEmail(registerBinding.createUserName.text.toString(),
                registerBinding.createPass.text.toString())
            }
            //biométrico
        }
    }

    private fun goToCreateAccountWithEmail(email: String, password: String){
        if (!(requireActivity() as LoginActivity).validateEmail(email)) {
            Toast.makeText(requireContext(), getString(R.string.invalid_email), Toast.LENGTH_LONG).show()
        } else if (!(requireActivity() as LoginActivity).validatePassword(password)) {
            Toast.makeText(requireContext(), getString(R.string.invalid_pass), Toast.LENGTH_LONG).show()
        } else {
            (requireActivity() as LoginActivity).createNewUser(email, password)
        }
    }

}