package mario.adrgm.loginwithfirebase.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager

class SharedPreferencesApp(context: Context) {
    private var sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    fun writeStringData(tag: String, data: String) {
        sharedPreferences.edit {
            this.putString(tag, data)
            this.apply()
        }
    }

    fun getStringData(tag: String): Any? {
        return sharedPreferences.getString(tag, Constants.NO_DATA)
    }

    fun deleteData(tag: String) {
        sharedPreferences.edit {
            this.remove(tag)
        }
    }
}