package mario.adrgm.loginwithfirebase.utils

object Constants {

    const val EMAIL = "email"
    const val ID = "id"
    //Valores no encontrados
    const val NO_DATA = "no data found"
    const val USER = "user"
    const val NAME = "name"
    const val PRIMARY = "primaryMuscles"
    const val SECONDARY = "secondaryMuscles"
    const val URL = "url"
    const val NAME_OR_TIMESTAMP = "nameOrTimeStamp"
    const val REGISTER = "registerTraining"
    const val UNILATERAL = "unilateral"
}