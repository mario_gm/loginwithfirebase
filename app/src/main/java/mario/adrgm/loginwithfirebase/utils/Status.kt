package mario.adrgm.loginwithfirebase.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}

