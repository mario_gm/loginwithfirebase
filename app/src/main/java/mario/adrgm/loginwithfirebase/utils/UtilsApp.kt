package mario.adrgm.loginwithfirebase.utils

import android.text.Editable
import android.widget.EditText
import android.widget.Spinner

object UtilsApp {

    private val GENERIC = "Selecciona los músculos"

    fun listenSpinnerExercises(spinner: Spinner, editableText: EditText) {
        val anyThing = spinner.selectedItem
        val newString = if (editableText.text.toString().contains(anyThing.toString())) {
            removeWord(editableText.text.toString(), anyThing.toString())
        } else {
            if (!anyThing.equals(GENERIC)) {
                if (editableText.text.toString().isNotBlank()) {
                    StringBuilder(editableText.text).append(", ").append(anyThing).toString()
                } else {
                    anyThing.toString()
                }
            } else {
                editableText.text.toString()
            }
        }
        editableText.text = Editable.Factory.getInstance().newEditable(newString)
    }

    private fun removeWord(longString: String, partOfString: String): String {
        val words = longString.split(", ") // Separamos las palabras por la coma y el espacio
        val newWords = words.filter { it != partOfString } // Creamos una nueva lista sin la palabra a remover
        return newWords.joinToString(", ") // Unimos las palabras con coma y espacio
    }

    fun setItems(listItem: List<String>): String {
        if (listItem.size == 1) {
            return listItem[0]
        }
        return listItem.joinToString(separator = ", ")
    }

    fun getItems(cadena: String): List<String> {
        return cadena.split(",\\s*".toRegex())
    }

    fun compareLists(list1: List<String>, list2: List<String>): Boolean {
        val set = mutableSetOf<String>()
        set.addAll(list1)
        set.addAll(list2)
        return set.size == list1.size + list2.size
    }

}