package mario.adrgm.loginwithfirebase.utils

import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage

object StorageCloud {

    val storageCloud = Firebase.storage.reference
}