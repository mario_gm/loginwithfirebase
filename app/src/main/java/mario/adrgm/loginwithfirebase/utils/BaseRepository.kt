package mario.adrgm.loginwithfirebase.utils

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.ktx.firestoreSettings

open class BaseRepository {

    var dataBase: FirebaseFirestore = DatabaseAccess.getDataBase()

    private val settings = firestoreSettings {
        isPersistenceEnabled = true
        setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED).build()
    }

    init {
        dataBase.firestoreSettings = settings
    }
}