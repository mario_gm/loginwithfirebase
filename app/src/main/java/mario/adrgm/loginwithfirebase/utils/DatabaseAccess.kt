package mario.adrgm.loginwithfirebase.utils

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

object DatabaseAccess {

    fun getDataBase(): FirebaseFirestore {
        return Firebase.firestore
    }

    enum class CollectionsDataBase constructor(var type: String) {
        EXERCISES("Ejercicios"),
        ROUTINES("Rutinas"),
    }
}