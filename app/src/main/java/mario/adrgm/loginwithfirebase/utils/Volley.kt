package mario.adrgm.loginwithfirebase.utils

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley.newRequestQueue
import org.json.JSONException
import org.json.JSONObject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class Volley(context: Context) {

    val queue: RequestQueue = newRequestQueue(context)
    val volleyTagError  = "ErrorVolley"

    suspend fun getTimeInMexicoCity(url: String): String {
        return suspendCoroutine { continuation ->
            val stringRequest = StringRequest(Request.Method.GET, url,
                { response ->
                    // Parse the JSON response and extract the current time
                    try {
                        val jsonObject = JSONObject(response)
                        val currentTime = jsonObject.getString("datetime")
                        // Return the current time
                        continuation.resume(currentTime)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        val allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+×÷=/_$&*:\"'-,?¿_"
                        val randomString = (1..28)
                            .map { allowedChars.random() }
                            .joinToString("")
                        // Return null if there was an error parsing the response
                        e.localizedMessage?.let { Log.d(volleyTagError, it) }
                        continuation.resume(randomString)
                    }
                },
                { error ->
                    // Handle error
                    val allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+×÷=_$&*:'-,?¿_"
                    val randomString = (1..28)
                        .map { allowedChars.random() }
                        .joinToString("")
                    println(randomString)
                    error.networkResponse.let { Log.d(volleyTagError, it?.toString() ?: "null") }
                    continuation.resume(randomString)
                })

            // Add the request to the RequestQueue.
            queue.add(stringRequest)
        }
    }
}