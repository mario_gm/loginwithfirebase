package mario.adrgm.loginwithfirebase.utils

import mario.adrgm.loginwithfirebase.login.LoginViewModel
import java.net.InetAddress


class CheckConnection {

    companion object {
        var isInternet = false
    }

    fun isInternetAvailable(loginViewModel: LoginViewModel) {
        val thread = Thread {
            try {
                connectToGoogle()
            } catch (e: java.lang.Exception) {
                isInternet = false
                e.printStackTrace()
            } finally {
                loginViewModel.setConnectionVariable(isInternet)
            }
        }
        thread.start()
    }

    private fun connectToGoogle() {
        val ipAddress: InetAddress = InetAddress.getByName("google.com")
        isInternet = !ipAddress.equals("")
    }
}