package mario.adrgm.loginwithfirebase.account

import android.content.Intent
import android.os.Bundle
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import mario.adrgm.loginwithfirebase.R
import mario.adrgm.loginwithfirebase.databinding.ActivityOptionsBinding
import mario.adrgm.loginwithfirebase.exercises.ExercisesActivity
import mario.adrgm.loginwithfirebase.login.LoginActivity
import mario.adrgm.loginwithfirebase.login.LoginViewModel
import mario.adrgm.loginwithfirebase.routines.SessionActivity
import mario.adrgm.loginwithfirebase.utils.SharedPreferencesApp


class OptionsActivity : AppCompatActivity() {

    private lateinit var listView: ListView
    private lateinit var optionsBinding: ActivityOptionsBinding
    private val loginViewModel: LoginViewModel by viewModels()

    private enum class ListOptions constructor(val value: Int) {
        EXERCISES(0),
        ROUTINES(1),
        PROFILE(2),
        LOGOUT(3)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        optionsBinding = ActivityOptionsBinding.inflate(layoutInflater)
        setContentView(optionsBinding.root)
        initList()
    }

    private fun initList(){
        val adapter: ArrayAdapter<*> = ArrayAdapter(
            this,
            R.layout.options_list_view, getValues()
        )
        listView = optionsBinding.options
        listView.adapter = adapter
        setListeners()
    }

    private fun getValues(): List<String> {
        return listOf(getString(R.string.exercises), getString(R.string.training),
             getString(R.string.profile), getString(R.string.log_out))
    }

    private fun setListeners() {
        listView.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            when(position) {
                ListOptions.EXERCISES.value -> {
                    val intentToExercises = Intent(this, ExercisesActivity::class.java)
                    startActivity(intentToExercises)
                }
                ListOptions.ROUTINES.value -> {
                    val intentToExercises = Intent(this, SessionActivity::class.java)
                    startActivity(intentToExercises)
                }
                ListOptions.PROFILE.value -> {

                }
                ListOptions.LOGOUT.value -> {
                    loginViewModel.logOut(SharedPreferencesApp(this))
                    val intentToLogin = Intent(this, LoginActivity::class.java)
                    finish()
                    startActivity(intentToLogin)
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}