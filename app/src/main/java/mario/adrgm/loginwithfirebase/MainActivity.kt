package mario.adrgm.loginwithfirebase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import mario.adrgm.loginwithfirebase.utils.CheckConnection
import mario.adrgm.loginwithfirebase.databinding.ActivityMainBinding
import mario.adrgm.loginwithfirebase.login.LoginActivity
import mario.adrgm.loginwithfirebase.login.LoginViewModel

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var checkConnection = CheckConnection()
    private val loginViewModel: LoginViewModel by viewModels()
    private lateinit var bindingMain: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingMain = ActivityMainBinding.inflate(layoutInflater)
        setContentView(bindingMain.root)
        setListeners()
        verifyIfInternetIsAvailable()
        subscribeLiveData()
    }

    private fun setListeners(){
        bindingMain.retrySplash.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        verifyIfInternetIsAvailable()
    }

    private fun verifyIfInternetIsAvailable(){
        bindingMain.progressBar.visibility = View.VISIBLE
        checkConnection.isInternetAvailable(loginViewModel)
    }

    private fun subscribeLiveData(){
        loginViewModel.connection.observe(this){ isConnectionOnDevice ->
            bindingMain.progressBar.visibility = View.GONE
            if (isConnectionOnDevice) { //goToLogin
                val intentToLogin = Intent(this, LoginActivity::class.java)
                finish()
                startActivity(intentToLogin)
            } else { //showToast
                Toast.makeText(this, getString(R.string.no_internet), Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onClick(view: View?) {
        when(view?.id){
            R.id.retry_splash -> {
                verifyIfInternetIsAvailable()
            }
        }
    }
}