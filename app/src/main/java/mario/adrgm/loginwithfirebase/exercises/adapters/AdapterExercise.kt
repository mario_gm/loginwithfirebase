package mario.adrgm.loginwithfirebase.exercises.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mario.adrgm.loginwithfirebase.R
import mario.adrgm.loginwithfirebase.exercises.ExercisesActivity
import mario.adrgm.loginwithfirebase.exercises.ExercisesModel

class AdapterExercise(
    var exercisesList: MutableList<ExercisesModel>,
    private val exercisesActivity: ExercisesActivity
) : RecyclerView.Adapter<AdapterExercise.ViewHolder>() {


    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val textView: TextView = itemView.findViewById(R.id.name_item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_layout, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = exercisesList[position]

        // sets the text to the textview from our itemHolder class
        holder.textView.text = item.name

        holder.itemView.setOnClickListener {
            when {
                exercisesActivity.isRetrievingData() -> {
                    exercisesActivity.goToDataExercise(item)
                }
                else -> {
                    exercisesActivity.goToCreateExercise(holder.itemView, item)
                }
            }
        }
    }


    override fun getItemCount(): Int {
        return exercisesList.size
    }

    fun updateList(newList: MutableList<ExercisesModel>) {
        exercisesList = newList
        notifyDataSetChanged()
    }
}