package mario.adrgm.loginwithfirebase.exercises
import android.os.Parcel
import android.os.Parcelable
import mario.adrgm.loginwithfirebase.registertraining.RegisterTraining

data class ExercisesModel(
    val id: String,
    var user: String?,
    var name: String,
    var primaryMuscles: List<String>,
    var secondaryMuscles: List<String>,
    var unilateral: Boolean,
    var registerTraining: ArrayList<RegisterTraining>?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString(),
        parcel.readString()!!,
        parcel.createStringArrayList()!!,
        parcel.createStringArrayList()!!,
        parcel.readByte() != 0.toByte(),
        parcel.createTypedArrayList(RegisterTraining) as ArrayList<RegisterTraining>
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(user)
        parcel.writeString(name)
        parcel.writeStringList(primaryMuscles)
        parcel.writeStringList(secondaryMuscles)
        parcel.writeByte(if (unilateral) 1 else 0)
        parcel.writeTypedList(registerTraining)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ExercisesModel> {
        override fun createFromParcel(parcel: Parcel): ExercisesModel {
            return ExercisesModel(parcel)
        }

        override fun newArray(size: Int): Array<ExercisesModel?> {
            return arrayOfNulls(size)
        }
    }
}

enum class Muscles(val nameMuscle: String) {
    GENERIC("Selecciona los músculos"),
    PECHO("Pecho"),
    ESPALDA("Espalda"),
    HOMBRO("Hombro"),
    BICEPS("Bíceps"),
    TRICEPS("Tríceps"),
    ABDOMINALES("Abdominales"),
    CUADRICEPS("Cuadríceps"),
    FEMORALES("Femorales"),
    GLUTEOS("Glúteos"),
    GEMELOS("Gemelos")
}
