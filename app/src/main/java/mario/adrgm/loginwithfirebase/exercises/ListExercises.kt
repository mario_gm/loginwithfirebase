package mario.adrgm.loginwithfirebase.exercises

import mario.adrgm.loginwithfirebase.utils.Status

data class ListExercises(
    val listExercises: List<ExercisesModel>?,
    var status: Status
)
