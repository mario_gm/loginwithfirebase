package mario.adrgm.loginwithfirebase.exercises

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import mario.adrgm.loginwithfirebase.account.OptionsActivity
import mario.adrgm.loginwithfirebase.databinding.ActivityExercisesBinding
import mario.adrgm.loginwithfirebase.registertraining.RegisterActivity
import mario.adrgm.loginwithfirebase.routines.RoutineModel
import mario.adrgm.loginwithfirebase.routines.SessionActivity
import mario.adrgm.loginwithfirebase.utils.SharedPreferencesApp

class ExercisesActivity : AppCompatActivity() {

    private lateinit var bindingExercisesBinding: ActivityExercisesBinding
    lateinit var sharedPreferencesApp: SharedPreferencesApp
    lateinit var routine: RoutineModel
    private var registerRoutine = false
    private var addingRoutine = false
    private var retrieveData = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingExercisesBinding = ActivityExercisesBinding.inflate(layoutInflater)
        setContentView(bindingExercisesBinding.root)
        sharedPreferencesApp = SharedPreferencesApp(this)
        registerRoutine = intent.getBooleanExtra(SessionActivity.CREATE_ROUTINE, false)
        retrieveData = intent.getBooleanExtra(SessionActivity.RETRIEVE_DATA, false)
    }

    companion object {
        var isInList = false
        const val SESSION = "Session"
        var isEditingRoutine = false
        var exerciseInQuestion: ExercisesModel? = null
    }

    fun goToCreateExercise(view: View, exercise: ExercisesModel?) {
        val action =
            ListExercisesFragmentDirections.actionListExercisesFragmentToAddExerciseFragment(
                exercise
            )
        view.findNavController().navigate(action)
    }

    fun isRetrievingData(): Boolean {
        return retrieveData
    }

    fun goToDataExercise(registerTraining: ExercisesModel) {
        val intentToData = Intent(this, RegisterActivity::class.java)
        exerciseInQuestion = registerTraining
        startActivity(intentToData)
    }

    private fun getOrderLetter(plus: Int): String {
        var letra = 'A'
        val suma = 0 + plus
        letra += suma - 1
        return letra.toString()
    }

    fun returnToList(view: View) {
        val action =
            AddExerciseFragmentDirections.actionAddExerciseFragmentToListExercisesFragment()
        view.findNavController().navigate(action)
    }

    fun showLoader() {
        bindingExercisesBinding.progressBar.visibility = View.VISIBLE
    }

    fun dismissLoader() {
        bindingExercisesBinding.progressBar.visibility = View.GONE
    }

    private fun goToOptionsActivity() {
        val intentToOptions = Intent(this, OptionsActivity::class.java)
        finish()
        startActivity(intentToOptions)
    }

    override fun onBackPressed() {
        if (isInList) {
            goToOptionsActivity()
        } else {
            super.onBackPressed()
        }
    }
}