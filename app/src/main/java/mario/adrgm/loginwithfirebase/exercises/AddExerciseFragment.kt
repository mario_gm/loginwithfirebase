package mario.adrgm.loginwithfirebase.exercises

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch
import mario.adrgm.loginwithfirebase.R
import mario.adrgm.loginwithfirebase.databinding.FragmentAddExerciseBinding
import mario.adrgm.loginwithfirebase.utils.Constants
import mario.adrgm.loginwithfirebase.utils.Status
import mario.adrgm.loginwithfirebase.utils.UtilsApp
import mario.adrgm.loginwithfirebase.utils.UtilsApp.getItems
import mario.adrgm.loginwithfirebase.utils.UtilsApp.listenSpinnerExercises
import mario.adrgm.loginwithfirebase.utils.Volley
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val EXERCISE = "exercise"

/**
 * A simple [Fragment] subclass.
 * Use the [AddExerciseFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AddExerciseFragment : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    private var bindingAddExercisesBinding: FragmentAddExerciseBinding? = null
    private val addExercisesBinding get() = bindingAddExercisesBinding!!
    private val exercisesViewModel: ExercisesViewModel by viewModels()
    private var exerciseParameter: ExercisesModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            exerciseParameter = it.getParcelable(EXERCISE)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        bindingAddExercisesBinding = FragmentAddExerciseBinding.inflate(inflater, container, false)
        return addExercisesBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        fillSpinners()
        setView()
        subscribeLiveData()
    }

    private fun setView() {
        if (exerciseParameter != null) {
            addExercisesBinding.deleteButtonDb.visibility = View.VISIBLE
            addExercisesBinding.addButtonDb.text = getString(R.string.edit_exercise)
            fillDataFromExistingExercise()
        }
    }

    private fun fillDataFromExistingExercise() {
        addExercisesBinding.exerciseName.setText(exerciseParameter?.name ?: "")
        addExercisesBinding.primaryText.setText(exerciseParameter?.let { UtilsApp.setItems(it.primaryMuscles) })
        addExercisesBinding.secondaryText.setText(exerciseParameter?.let { UtilsApp.setItems(it.secondaryMuscles) })
        addExercisesBinding.boxUnilateral.isChecked = exerciseParameter?.unilateral == true
    }

    private fun subscribeLiveData() {
        exercisesViewModel.liveExerciseAdd.observe(viewLifecycleOwner) {
            when (it) {
                Status.LOADING -> {
                    addExercisesBinding.viewAddExercise.isEnabled = false
                    (requireActivity() as ExercisesActivity).showLoader()
                }
                Status.ERROR, null -> {
                    (requireActivity() as ExercisesActivity).dismissLoader()
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.not_valid_firebase_save),
                        Toast.LENGTH_LONG
                    ).show()
                }
                Status.SUCCESS -> {
                    (requireActivity() as ExercisesActivity).dismissLoader()
                    addExercisesBinding.viewAddExercise.isEnabled = true
                    (requireActivity() as ExercisesActivity).returnToList(requireView())
                }
            }
        }
        exercisesViewModel.liveExerciseUpdate.observe(viewLifecycleOwner) {
            when (it) {
                Status.LOADING -> {
                    addExercisesBinding.viewAddExercise.isEnabled = false
                    (requireActivity() as ExercisesActivity).showLoader()
                }
                Status.ERROR -> {
                    (requireActivity() as ExercisesActivity).dismissLoader()
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.not_valid_firebase_save),
                        Toast.LENGTH_LONG
                    ).show()
                }
                Status.SUCCESS -> {
                    (requireActivity() as ExercisesActivity).dismissLoader()
                    addExercisesBinding.viewAddExercise.isEnabled = true
                    (requireActivity() as ExercisesActivity).returnToList(requireView())
                }
            }
        }
        exercisesViewModel.liveExerciseDelete.observe(viewLifecycleOwner) {
            when (it) {
                Status.ERROR -> {
                    (requireActivity() as ExercisesActivity).dismissLoader()
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.not_valid_firebase_delete),
                        Toast.LENGTH_LONG
                    ).show()
                }
                Status.SUCCESS -> {
                    (requireActivity() as ExercisesActivity).dismissLoader()
                    addExercisesBinding.viewAddExercise.isEnabled = true
                    (requireActivity() as ExercisesActivity).returnToList(requireView())
                }
                Status.LOADING -> {
                    addExercisesBinding.viewAddExercise.isEnabled = false
                    (requireActivity() as ExercisesActivity).showLoader()
                }
            }
        }
    }

    private fun setListeners() {
        addExercisesBinding.addButtonDb.setOnClickListener(this)
        addExercisesBinding.cancelButtonAdd.setOnClickListener(this)
        addExercisesBinding.deleteButtonDb.setOnClickListener(this)
        addExercisesBinding.addPrimary.onItemSelectedListener = this
        addExercisesBinding.addSecondary.onItemSelectedListener = this
    }


    private fun fillSpinners() {
        val muscles = Muscles.values().map { it.nameMuscle }
        val adapter = ArrayAdapter(
            addExercisesBinding.addPrimary.context,
            android.R.layout.simple_spinner_item, muscles
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        addExercisesBinding.addPrimary.adapter = adapter
        val adapter2 = ArrayAdapter(
            addExercisesBinding.addSecondary.context,
            android.R.layout.simple_spinner_item, muscles
        )
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        addExercisesBinding.addSecondary.adapter = adapter2
    }

    companion object {
        @JvmStatic
        fun newInstance(exercisesModel: ExercisesModel?) =
            AddExerciseFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(EXERCISE, exercisesModel)
                }
            }
    }

    private fun validateInfo() {
        val list1 = getItems(addExercisesBinding.primaryText.text.toString())
        val list2 = getItems(addExercisesBinding.secondaryText.text.toString())
        if (addExercisesBinding.exerciseName.text.isBlank() or addExercisesBinding.primaryText.text.isBlank()) {
            Toast.makeText(
                requireContext(),
                getString(R.string.not_valid_info_exercise),
                Toast.LENGTH_LONG
            ).show()
        } else if (!UtilsApp.compareLists(list1, list2)) {
            Toast.makeText(
                requireContext(),
                getString(R.string.duplicate_muscles),
                Toast.LENGTH_LONG
            ).show()
        } else if (exerciseParameter == null) {
            lifecycleScope.launch {
                exercisesViewModel.addExercise(createExercise(list1, list2, false))
            }
        } else {
            lifecycleScope.launch {
                try {
                    exercisesViewModel.updateExercise(createExercise(list1, list2, true))
                } catch (e: NullPointerException) {
                    e.localizedMessage?.let { Log.d("Fallo al actualizar", it) }
                }
            }
        }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            addExercisesBinding.addButtonDb.id -> {
                validateInfo()
            }
            addExercisesBinding.cancelButtonAdd.id -> {
                (requireActivity() as ExercisesActivity).returnToList(view)
            }
            addExercisesBinding.deleteButtonDb.id -> {
                showAlertDialogYesOrNo()
            }
        }
    }

    private fun deleteData() {
        lifecycleScope.launch {
            exercisesViewModel.deleteData(
                createExercise(
                    getItems(addExercisesBinding.primaryText.text.toString()),
                    getItems(addExercisesBinding.secondaryText.text.toString()),
                    true
                )
            )
        }
    }

    private fun showAlertDialogYesOrNo() {
        val builderAlert: AlertDialog.Builder = AlertDialog.Builder(context)
        builderAlert.setMessage(getString(R.string.confirm_delete_exercise))
        builderAlert.setCancelable(true)

        builderAlert.setPositiveButton(
            "No"
        ) { dialog, id ->
            dialog.cancel()
        }

        builderAlert.setNegativeButton(
            "Sí"
        ) { dialog, id ->
            deleteData()
            dialog.cancel()
        }

        val alertShow: AlertDialog = builderAlert.create()
        alertShow.show()
    }

    private suspend fun createExercise(
        list1: List<String>,
        list2: List<String>,
        updateOrDelete: Boolean
    ): ExercisesModel {
        val user =
            (requireActivity() as ExercisesActivity).sharedPreferencesApp.getStringData(Constants.EMAIL)
                .toString()
        val nameEx = addExercisesBinding.exerciseName.text.toString()
        val id = setId(user, updateOrDelete)
        return ExercisesModel(
            id!!,
            user,
            nameEx,
            list1,
            list2,
            addExercisesBinding.boxUnilateral.isChecked,
            registerTraining = if (updateOrDelete) {exerciseParameter?.registerTraining} else {null}
        )
    }

    private suspend fun setId(user: String, update: Boolean): String? {
        return if (update) {
            exerciseParameter?.id
        } else {
            val timeId = Volley(requireContext()).getTimeInMexicoCity(getString(R.string.get_time))
            StringBuilder(user).append(timeId).toString()
        }
    }


    override fun onItemSelected(adapterView: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        when (adapterView?.id) {
            addExercisesBinding.addPrimary.id -> {
                listenSpinnerExercises(
                    addExercisesBinding.addPrimary,
                    addExercisesBinding.primaryText
                )
            }
            addExercisesBinding.addSecondary.id -> {
                listenSpinnerExercises(
                    addExercisesBinding.addSecondary,
                    addExercisesBinding.secondaryText
                )
            }
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        //do nothing
    }


}