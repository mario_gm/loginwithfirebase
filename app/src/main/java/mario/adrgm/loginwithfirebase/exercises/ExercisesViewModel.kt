package mario.adrgm.loginwithfirebase.exercises

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mario.adrgm.loginwithfirebase.utils.Status

class ExercisesViewModel: ViewModel() {

    private val exercisesRepository by lazy {
        ExercisesRepository()
    }

    val liveExerciseAdd = MutableLiveData<Status>()
    val liveExerciseGetter = MutableLiveData<ListExercises>()
    val liveExerciseUpdate = MutableLiveData<Status>()
    val liveExerciseDelete = MutableLiveData<Status>()
    fun addExercise(exerciseModel: ExercisesModel) {
        viewModelScope.launch {
            liveExerciseAdd.value = Status.LOADING
            val resp = exercisesRepository.saveExercise(exerciseModel)
            withContext(Dispatchers.IO) {
                liveExerciseAdd.postValue(
                    if (resp) {
                        Status.SUCCESS
                    } else {
                        Status.ERROR
                    }
                )
            }
        }
    }

    fun updateExercise(exerciseModel: ExercisesModel) {
        viewModelScope.launch {
            liveExerciseUpdate.value = Status.LOADING
            val resp = exercisesRepository.updateExercise(exerciseModel)
            withContext(Dispatchers.IO) {
                liveExerciseUpdate.postValue(
                    if (resp) {
                        Status.SUCCESS
                    } else {
                        Status.ERROR
                    }
                )
            }
        }
    }

    fun getAllDataFromUser(email: String) {
        liveExerciseGetter.value = ListExercises(null, Status.LOADING)
        viewModelScope.launch {
            val resp = exercisesRepository.getExercisesByUser(email)
            withContext(Dispatchers.IO) {
                liveExerciseGetter.postValue(resp)
            }
        }
    }

    fun deleteData(exerciseModel: ExercisesModel) {
        liveExerciseDelete.value = Status.LOADING
        viewModelScope.launch {
            val resp = exercisesRepository.deleteExercise(exerciseModel)
            withContext(Dispatchers.IO) {
                liveExerciseDelete.postValue(
                    if (resp) {
                        Status.SUCCESS
                    } else {
                        Status.ERROR
                    }
                )
            }
        }
    }

}