package mario.adrgm.loginwithfirebase.exercises

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import mario.adrgm.loginwithfirebase.R
import mario.adrgm.loginwithfirebase.databinding.FragmentListExercisesBinding
import mario.adrgm.loginwithfirebase.exercises.adapters.AdapterExercise
import mario.adrgm.loginwithfirebase.routines.*
import mario.adrgm.loginwithfirebase.utils.Constants
import mario.adrgm.loginwithfirebase.utils.Status
import mario.adrgm.loginwithfirebase.utils.Volley

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ListExercisesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ListExercisesFragment : Fragment(), View.OnClickListener {

    private lateinit var timeStamp: String
    private lateinit var adapter: AdapterExercise
    private lateinit var recyclerView: RecyclerView
    private var bindingListExercisesBinding: FragmentListExercisesBinding? = null
    private val listExercisesBinding get() = bindingListExercisesBinding!!
    private lateinit var listShowExercisesModel: MutableList<ExercisesModel>
    private lateinit var initList: MutableList<ExercisesModel>
    private val exercisesViewModel: ExercisesViewModel by viewModels()
    private val routineViewModel: RoutineViewModel by viewModels()
    private var param1: String? = null
    private var param2: String? = null
    private var nameSession: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        bindingListExercisesBinding = FragmentListExercisesBinding.inflate(inflater, container, false)
        return listExercisesBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = listExercisesBinding.recyclerExercises
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        fillData()
        setListeners()
        subscribeLiveData()
    }


    private fun subscribeLiveData() {
        exercisesViewModel.liveExerciseGetter.observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> {
                    (requireActivity() as ExercisesActivity).showLoader()
                }
                Status.SUCCESS -> {
                    (requireActivity() as ExercisesActivity).dismissLoader()
                    initList = it.listExercises as MutableList<ExercisesModel>
                    listShowExercisesModel = initList
                    adapter = AdapterExercise(
                        listShowExercisesModel,
                        requireActivity() as ExercisesActivity
                    )
                    recyclerView.adapter = adapter
                }
                Status.ERROR -> {
                    Toast.makeText(requireContext(), getString(R.string.not_retrieve), Toast.LENGTH_LONG).show()
                    (requireActivity() as ExercisesActivity).dismissLoader()
                }
            }
        }
        routineViewModel.registerTraining.observe(viewLifecycleOwner) {
            when (it) {
                Status.LOADING -> {
                    (requireActivity() as ExercisesActivity).showLoader()
                }
                Status.ERROR, null -> {
                    (requireActivity() as ExercisesActivity).dismissLoader()
                    Toast.makeText(requireContext(), getString(R.string.error_database), Toast.LENGTH_LONG).show()
                }
                Status.SUCCESS -> {
                    (requireActivity() as ExercisesActivity).dismissLoader()
                }
            }
        }
    }

    private fun fillData() {
        (requireActivity() as ExercisesActivity).sharedPreferencesApp.getStringData(Constants.EMAIL)
            ?.let { exercisesViewModel.getAllDataFromUser(it.toString()) }
    }

    private fun setListeners() {
        listExercisesBinding.buttonAddExercise.setOnClickListener(this)
        listExercisesBinding.buttonRegisterTraining.setOnClickListener(this)
        searchViewHandler()
    }

    override fun onResume() {
        super.onResume()
        ExercisesActivity.isInList = true
    }

    override fun onPause() {
        super.onPause()
        ExercisesActivity.isInList = false
    }

    private fun searchViewHandler() {
        listExercisesBinding.searchExercise.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText.isNullOrEmpty()) {
                    if (this@ListExercisesFragment::initList.isInitialized)
                        listShowExercisesModel = initList
                } else {
                    listShowExercisesModel =
                        initList.filter { it.name.contains(newText, ignoreCase = true) } as MutableList<ExercisesModel>
                }
                if (this@ListExercisesFragment::listShowExercisesModel.isInitialized)
                    adapter.updateList(listShowExercisesModel)
                return true
            }
        })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ListExercisesFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ListExercisesFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }

        var routineWithExercises: RoutineModel? = null
        var updateAdapter = false
    }



    private fun addName(callback: () -> Unit) {
        val builderAlert: AlertDialog.Builder = AlertDialog.Builder(requireContext())
        builderAlert.setMessage(getString(R.string.name_routine))
        builderAlert.setCancelable(false)

        // Set up the input
        val input = EditText(requireContext())
        input.inputType = InputType.TYPE_CLASS_TEXT
        builderAlert.setView(input)

        builderAlert.setPositiveButton(
            "No darle nombre"
        ) { dialog, id ->
            callback()
            dialog.dismiss()
        }

        builderAlert.setNegativeButton(
            "Asignar nombre"
        ) { dialog, id ->
            if (input.text.toString().isNotEmpty()) {
                nameSession = input.text.toString()
            }
            callback() // llamar al parámetro de devolución de llamada
            dialog.dismiss()
        }

        val alertShow: AlertDialog = builderAlert.create()
        alertShow.show()
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            listExercisesBinding.buttonAddExercise.id -> {
                (requireActivity() as ExercisesActivity).goToCreateExercise(view, null)
            }
            listExercisesBinding.buttonRegisterTraining.id -> {
                var nameEntered = false // variable para verificar si se ha ingresado un nombre
                addName {
                    nameEntered = true // establecer la variable en true cuando se completa el diálogo
                }
                lifecycleScope.launch {
                    while (!nameEntered) { // esperar hasta que se haya ingresado un nombre
                        delay(100) // esperar 100 milisegundos antes de verificar de nuevo
                    }
                    routineViewModel.newRoutine((requireActivity() as ExercisesActivity).routine)
                }
            }
        }
    }

}