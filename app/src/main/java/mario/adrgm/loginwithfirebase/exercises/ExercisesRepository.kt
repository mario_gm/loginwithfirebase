package mario.adrgm.loginwithfirebase.exercises

import android.util.Log
import kotlinx.coroutines.tasks.await
import mario.adrgm.loginwithfirebase.registertraining.RegisterTraining
import mario.adrgm.loginwithfirebase.utils.*

class ExercisesRepository: BaseRepository() {

    private val tag = "ExerciseRepository"

    suspend fun saveExercise(exercise: ExercisesModel): Boolean {
        return try {
            // Guarda el objeto en Firestore
                dataBase.collection(DatabaseAccess.CollectionsDataBase.EXERCISES.type).document(exercise.id)
                    .set(exercise)
                    .await()

            Log.d(tag, "Objeto guardado con ID: ${exercise.name}")
            true
        } catch (e: Exception) {
            Log.d(tag, "Error al guardar el objeto: $e")
            false
        }
    }


    suspend fun getExercisesByUser(email: String): ListExercises {
        try {
            val ref = dataBase.collection(DatabaseAccess.CollectionsDataBase.EXERCISES.type)
                .whereEqualTo(Constants.USER, email)
                .get().await().documents
            Log.d(tag, "Objetos recuperados: $ref")
            val listData = ref.map { item ->
                ExercisesModel(
                    item[Constants.ID].toString(),
                    item[Constants.USER].toString(),
                    item[Constants.NAME].toString(),
                    item[Constants.PRIMARY] as List<String>,
                    item[Constants.SECONDARY] as List<String>,
                    item[Constants.UNILATERAL].toString().toBoolean(),
                    item[Constants.REGISTER] as ArrayList<RegisterTraining>?
                )
            }
            return ListExercises(listData, Status.SUCCESS)
        } catch (e: Exception) {
            Log.d(tag, "Error al recuperar el objeto: $e")
            return ListExercises(null, Status.ERROR)
        }
    }

    suspend fun updateExercise(exercise: ExercisesModel): Boolean {
        return try {
                dataBase.collection(DatabaseAccess.CollectionsDataBase.EXERCISES.type).document(exercise.id)
                    .set(exercise).await()
            Log.d(tag, "Actualizado: ${exercise.name}")
            true
        } catch (e: Exception) {
            Log.d(tag, "Error al actualizar: $e")
            false
        }
    }

    suspend fun deleteExercise(exercise: ExercisesModel): Boolean {
        return try {
            dataBase.collection(DatabaseAccess.CollectionsDataBase.EXERCISES.type).document(exercise.id).delete().await()
            Log.d(tag, "Borrado con exito: ${exercise.name}")
            true
        }catch (e: Exception) {
            Log.d(tag, "Error al borrar: $e")
            return false
        }
    }
}