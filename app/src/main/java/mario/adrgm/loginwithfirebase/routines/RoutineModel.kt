package mario.adrgm.loginwithfirebase.routines

import android.net.Uri
import android.os.Parcel
import android.os.Parcelable

data class RoutineModel(
    val id: String,
    var nameOrTimeStamp: String,
    var user: String,
    val url: String
) :  Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!
    )

    constructor() : this("", "", "", "")

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(nameOrTimeStamp)
        parcel.writeString(user)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RoutineModel> {
        override fun createFromParcel(parcel: Parcel): RoutineModel {
            return RoutineModel(parcel)
        }

        override fun newArray(size: Int): Array<RoutineModel?> {
            return arrayOfNulls(size)
        }
    }

}
