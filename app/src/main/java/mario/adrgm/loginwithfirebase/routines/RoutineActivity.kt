package mario.adrgm.loginwithfirebase.routines

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.provider.OpenableColumns
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import mario.adrgm.loginwithfirebase.R
import mario.adrgm.loginwithfirebase.databinding.ActivityRoutineBinding
import mario.adrgm.loginwithfirebase.exercises.ExercisesActivity
import mario.adrgm.loginwithfirebase.routines.adapters.RoutineAdapter
import mario.adrgm.loginwithfirebase.utils.*
import java.io.File
import kotlin.random.Random

class RoutineActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var bindingRoutineActivity: ActivityRoutineBinding
    lateinit var sharedPreferencesApp: SharedPreferencesApp
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: RoutineAdapter
    private val routineViewModel: RoutineViewModel by viewModels()
    private val PICK_FILE_REQUEST = 123

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingRoutineActivity = ActivityRoutineBinding.inflate(layoutInflater)
        setContentView(bindingRoutineActivity.root)
        sharedPreferencesApp = SharedPreferencesApp(this)
        recyclerView = bindingRoutineActivity.recyclerRoutines
        recyclerView.layoutManager = LinearLayoutManager(this)
        fillData()
        subscribeLiveData()
        setListeners()
    }

    private fun fillData() {
        routineViewModel.getRoutinesByUser(sharedPreferencesApp.getStringData(Constants.EMAIL).toString())
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intentToExercises = Intent(this, SessionActivity::class.java)
        startActivity(intentToExercises)
    }

    private fun showLoader() {
        bindingRoutineActivity.progressBar.visibility = View.VISIBLE
    }

    private fun dismissLoader() {
        bindingRoutineActivity.progressBar.visibility = View.INVISIBLE
    }

    private fun subscribeLiveData() {
        routineViewModel.getRoutines.observe(this) {
            when (it.status) {
                Status.LOADING -> {
                    showLoader()
                }
                Status.SUCCESS -> {
                    dismissLoader()
                    adapter = RoutineAdapter(
                        it.listOfRoutines as MutableList<RoutineModel>,
                        this
                    )
                    recyclerView.adapter = adapter
                }
                Status.ERROR -> {
                    Toast.makeText(this, getString(R.string.not_retrieve), Toast.LENGTH_LONG).show()
                    dismissLoader()
                }
            }
        }
    }

    private fun setListeners() {
        bindingRoutineActivity.buttonAddExercise.setOnClickListener(this)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_FILE_REQUEST && resultCode == Activity.RESULT_OK) {
            data?.data?.let { uri ->
                val email = sharedPreferencesApp.getStringData(
                    Constants.EMAIL
                ).toString()
                val childForUser = StorageCloud.storageCloud.child(
                    email
                )
                val childRoutine = childForUser.child("/routines")
                val reference = childRoutine.child(randomReference())
                val inputStream = contentResolver.openInputStream(uri)
                var download = Uri.EMPTY
                lifecycleScope.launch {
                    try {
                        Toast.makeText(this@RoutineActivity, "Estamos subiendo tu rutina, toma unos segundos", Toast.LENGTH_SHORT).show()
                        inputStream?.let { reference.putStream(it) }?.await()

                        // Handle successful upload

                        val downloadUrl = reference.downloadUrl.await()
                        download = downloadUrl

                        // Call createRoutineModel
                        createRoutineModel(email, download, getFileNameFromUri(uri) ?: "SinTitulo")
                    } catch (e: Exception) {
                        // Handle exception
                        Toast.makeText(this@RoutineActivity, "Fallo la carga, intentanlo más tarde", Toast.LENGTH_SHORT).show()
                    }
                }
            }

        }
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)

        // If there's an upload in progress, save the reference so you can query it later
        outState.putString("reference", StorageCloud.storageCloud.toString())
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        // If there was an upload in progress, get its reference and create a new StorageReference
        val stringRef = savedInstanceState.getString("reference") ?: return

        // Find all UploadTasks under this StorageReference (in this example, there should be one)

        val tasks = StorageCloud.storageCloud.activeUploadTasks

        if (tasks.size > 0) {
            // Get the task monitoring the upload
            val task = tasks[0]

            // Add new listeners to the task using an Activity scope
            task.addOnSuccessListener(this) {
                // Success!
                // ...
            }
        }
    }

    private suspend fun createRoutineModel(email: String, download: Uri, title: String) {
        val routineModel = RoutineModel(createIdForRoutine(email), title, email, download.toString())
        routineViewModel.newRoutine(routineModel)
        reloadActivity()
    }

    private fun reloadActivity() {
        finish()
        overridePendingTransition(0, 0)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    @SuppressLint("Range")
    private fun getFileNameFromUri(uri: Uri): String? {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = contentResolver.query(uri, null, null, null, null)
            cursor?.use {
                if (it.moveToFirst()) {
                    val displayName = it.getString(it.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                    if (!displayName.isNullOrEmpty()) {
                        result = displayName
                    }
                }
            }
        } else if (uri.scheme == "file") {
            result = File(uri.path).name
        }
        return result
    }

    private suspend fun createIdForRoutine(user: String): String {
        val timeStamp = Volley(this).getTimeInMexicoCity(getString(R.string.get_time))
        return StringBuilder(user).append(timeStamp).toString()
    }

    private fun randomReference(): String {
        val allowedCharacters = ('A'..'Z') + ('a'..'z') + ('0'..'9')
        val length = Random.nextInt(1, 1025) // Longitud aleatoria entre 1 y 1024 bytes

        val result = StringBuilder()
        repeat(length) {
            val randomChar = allowedCharacters.random()
            result.append(randomChar)
        }

        return result.toString()
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            bindingRoutineActivity.buttonAddExercise.id -> {
                val intentAddRoutine = Intent(Intent.ACTION_GET_CONTENT)
                intentAddRoutine.type = "*/*"
                startActivityForResult(intentAddRoutine, PICK_FILE_REQUEST)
            }
        }
    }
}