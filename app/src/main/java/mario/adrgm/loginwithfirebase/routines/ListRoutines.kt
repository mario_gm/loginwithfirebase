package mario.adrgm.loginwithfirebase.routines

import mario.adrgm.loginwithfirebase.utils.Status

data class ListRoutines(
    val listOfRoutines: List<RoutineModel>?,
    var status: Status
)
