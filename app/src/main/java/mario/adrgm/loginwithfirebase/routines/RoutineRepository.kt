package mario.adrgm.loginwithfirebase.routines

import android.util.Log
import kotlinx.coroutines.tasks.await
import mario.adrgm.loginwithfirebase.utils.BaseRepository
import mario.adrgm.loginwithfirebase.utils.Constants
import mario.adrgm.loginwithfirebase.utils.DatabaseAccess
import mario.adrgm.loginwithfirebase.utils.Status

class RoutineRepository : BaseRepository() {

    private val tag = "RoutineRepository"
    suspend fun createRoutine(routine: RoutineModel): Boolean {
        return try {
            dataBase.collection(DatabaseAccess.CollectionsDataBase.ROUTINES.type)
                .document(routine.id)
                .set(routine).await()
            Log.d(tag, "Objeto guardado con ID: ${routine.nameOrTimeStamp}")
            true
        } catch (e: Exception) {
            Log.d(tag, "Error al guardar el objeto: $e")
            false
        }
    }

    suspend fun getAllRoutinesByUser(emai: String): ListRoutines {
        return try {
            val ref = dataBase.collection(DatabaseAccess.CollectionsDataBase.ROUTINES.type)
                .whereEqualTo(Constants.USER, emai).get().await().documents
            Log.d(tag, "Objetos recuperados: $ref")
            val listData = ref.map { item ->
                RoutineModel(
                    item[Constants.ID].toString(),
                    item[Constants.NAME_OR_TIMESTAMP].toString(),
                    item[Constants.USER].toString(),
                    item[Constants.URL].toString()
                )
            }
            ListRoutines(listData, Status.SUCCESS)
        }catch (e: Exception) {
            e.localizedMessage?.let { Log.d(tag, it) }
            ListRoutines(null, Status.ERROR)
        }
    }

    suspend fun editRoutine(idRoutine: String, newName: String): Boolean {
        return try {
            //Obtener rutina
            val ref = dataBase.collection(DatabaseAccess.CollectionsDataBase.ROUTINES.type).document(idRoutine)
            val documentSnapShot = ref.get().await()
            val routine = documentSnapShot.toObject(RoutineModel::class.java)
            //Cambiar datos
            routine?.nameOrTimeStamp = newName
            //Guardar nuevo objeto en la base
            ref.update("nameOrTimeStamp", routine?.nameOrTimeStamp)
            true
        }catch (e: Exception) {
            e.localizedMessage?.let { Log.d("Fallo guardado: ", it) }
            false
        }
    }

}