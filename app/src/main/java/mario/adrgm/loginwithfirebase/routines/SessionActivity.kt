package mario.adrgm.loginwithfirebase.routines

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import mario.adrgm.loginwithfirebase.account.OptionsActivity
import mario.adrgm.loginwithfirebase.databinding.ActivitySessionBinding
import mario.adrgm.loginwithfirebase.exercises.ExercisesActivity
import mario.adrgm.loginwithfirebase.utils.SharedPreferencesApp

class SessionActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var sessionBinding: ActivitySessionBinding
    private val PICK_FILE_REQUEST = 123
    lateinit var sharedPreferencesApp: SharedPreferencesApp
    private val routineViewModel: RoutineViewModel by viewModels()

    companion object {
        const val CREATE_ROUTINE = "Register_Training"
        const val RETRIEVE_DATA = "Retrieve_Data"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sessionBinding = ActivitySessionBinding.inflate(layoutInflater)
        setContentView(sessionBinding.root)
        sharedPreferencesApp = SharedPreferencesApp(this)
        setListeners()
    }


    private fun setListeners() {
        sessionBinding.routines.setOnClickListener(this)
        sessionBinding.dataId.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            sessionBinding.routines.id -> {
                goToRoutine()
            }
            sessionBinding.dataId.id -> {
                gotToExercisesForRetrieveData()
            }
        }
    }
    private fun gotToExercisesForRetrieveData() {
        val intentToRetrieveData = Intent(this, ExercisesActivity::class.java)
        intentToRetrieveData.putExtra(RETRIEVE_DATA, true)
        startActivity(intentToRetrieveData)
    }

    private fun goToRoutine() {
        val intentToRoutines = Intent(this, RoutineActivity::class.java)
        startActivity(intentToRoutines)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intentToOptions = Intent(this, OptionsActivity::class.java)
        finish()
        startActivity(intentToOptions)
    }
}