package mario.adrgm.loginwithfirebase.routines.adapters

import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.RecyclerView
import mario.adrgm.loginwithfirebase.R
import mario.adrgm.loginwithfirebase.routines.*
import java.io.File

class RoutineAdapter(
    var routineList: MutableList<RoutineModel>,
    val context: Context
) : RecyclerView.Adapter<RoutineAdapter.ViewHolder>() {

    private var downloadId: Long = -1

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val textView: TextView = itemView.findViewById(R.id.name_item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_layout, parent, false)

        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = routineList[position]

        // sets the text to the textview from our itemHolder class
        holder.textView.text = item.nameOrTimeStamp

        holder.itemView.setOnClickListener {
            val url = item.url
            val fileName = item.nameOrTimeStamp
            val file = File(context.getExternalFilesDir(null), fileName)

            if (!file.exists()) {
                val downloadManager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

                val request = DownloadManager.Request(Uri.parse(url))
                    .setTitle(fileName)
                    .setDescription("Downloading file")
                    .setDestinationInExternalFilesDir(context, null, fileName)

                downloadId = downloadManager.enqueue(request)
                checkDownloadStatus(file)
            } else {
                tryToOpenFile(file)
            }

        }

    }

    private fun checkDownloadStatus(file: File) {
        val query = DownloadManager.Query().setFilterById(downloadId)
        val downloadManager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

        var downloading = true
        while (downloading) {
            val cursor = downloadManager.query(query)
            if (cursor.moveToFirst()) {
                when (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))) {
                    DownloadManager.STATUS_SUCCESSFUL -> {
                        // La descarga ha sido exitosa
                        downloading = false
                        tryToOpenFile(file)
                    }
                    DownloadManager.STATUS_FAILED -> {
                        downloading = false
                        Toast.makeText(context, "", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            cursor.close()
        }
    }

    private fun tryToOpenFile(file: File) {
        val fileUri = FileProvider.getUriForFile(context,
            "${context.applicationContext.packageName}.provider", file)
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(fileUri, "application/*")
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        if (intent.resolveActivity(context.packageManager) != null) {
            context.startActivity(intent)
        } else {
            Toast.makeText(context, "Ninguna aplicación encontrada para abrir el archivo", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int {
        return routineList.size
    }
}