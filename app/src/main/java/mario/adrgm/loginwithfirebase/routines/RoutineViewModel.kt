package mario.adrgm.loginwithfirebase.routines

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mario.adrgm.loginwithfirebase.utils.Status

class RoutineViewModel: ViewModel() {

    private val routineRepository by lazy {
        RoutineRepository()
    }

    val registerTraining = MutableLiveData<Status>()
    val getRoutines = MutableLiveData<ListRoutines>()
    val editRoutine = MutableLiveData<Status>()

    fun newRoutine(routine: RoutineModel) {
        viewModelScope.launch {
            registerTraining.value = Status.LOADING
            val resp = routineRepository.createRoutine(routine)
            withContext(Dispatchers.IO) {
                registerTraining.postValue(
                    if (resp) {
                        Status.SUCCESS
                    } else {
                        Status.ERROR
                    }
                )
            }
        }
    }

    fun getRoutinesByUser(email: String) {
        viewModelScope.launch {
            getRoutines.value = ListRoutines(null, Status.LOADING)
            val resp = routineRepository.getAllRoutinesByUser(email)
            withContext(Dispatchers.IO) {
                getRoutines.postValue(
                    resp
                )
            }
        }
    }

    fun editRoutine(idRoutine: String, newName: String) {
        viewModelScope.launch {
            editRoutine.value = Status.LOADING
            val resp = routineRepository.editRoutine(idRoutine, newName)
            withContext(Dispatchers.IO) {
                editRoutine.postValue(if (resp) {
                    Status.SUCCESS
                } else {
                    Status.ERROR
                })
            }
        }
    }

}