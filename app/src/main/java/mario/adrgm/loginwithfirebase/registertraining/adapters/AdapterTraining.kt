package mario.adrgm.loginwithfirebase.registertraining.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mario.adrgm.loginwithfirebase.R
import mario.adrgm.loginwithfirebase.exercises.ExercisesModel
import mario.adrgm.loginwithfirebase.registertraining.RegisterActivity
import mario.adrgm.loginwithfirebase.registertraining.RegisterTraining
import mario.adrgm.loginwithfirebase.registertraining.SetToRegister
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@Suppress("USELESS_IS_CHECK")
class AdapterTraining(
    var listRegisters: MutableList<RegisterTraining>,
    var exercisesModel: ExercisesModel,
    val registerActivity: RegisterActivity,
) : RecyclerView.Adapter<AdapterTraining.ViewHolder>() {
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val textView: TextView = itemView.findViewById(R.id.name_item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_layout, parent, false)

        return ViewHolder(view)
    }

    companion object {
        var registerTraining: RegisterTraining? = null

        fun clearRegister() {
            registerTraining = null
        }
    }

    private fun showSets(registerShow: RegisterTraining): String? {
        var number = StringBuilder()
        registerShow.set.forEach {
            number.append(it.pesoLbs).append("x").append(it.reps)
            if (it.reps2 != null)
                number.append("-").append(it.reps2)
            number.append(",")
        }
        number = number.removeRange(number.length - 1, number.length) as StringBuilder
        if (registerShow.annotations != "null")
            number.appendLine().append(registerShow.annotations)
        return number.toString()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = if (listRegisters[position] is RegisterTraining) {
            listRegisters[position]
        } else {
            returnValidRegister(listRegisters[position] as HashMap<*, *>)
        }

        holder.textView.text = StringBuilder(Date(item.date).toString()).appendLine().append(showSets(item))

        holder.itemView.setOnClickListener {
            registerTraining = item
            registerActivity.navigateToRegisterTraining(holder.itemView, exercisesModel.id)
        }
    }

    private fun returnValidRegister(hashMap: HashMap<*, *>): RegisterTraining {
        val mapArray = hashMap["set"] as ArrayList<*>
        val mapSeriesArray = ArrayList<SetToRegister>()
        for (j in 0 until mapArray.size) {
            val set = mapArray[j] as HashMap<*, *>
            val setToRegister = SetToRegister(
                set["reps"].toString().toInt(),
                set["reps2"].toString().toIntOrNull(),
                set["pesoLbs"].toString().toFloat(),
                set["pesoKg"].toString().toFloat(),
                set["desc"].toString().toInt())
            mapSeriesArray.add(
                setToRegister
            )
        }
        return RegisterTraining(hashMap["date"].toString().toLong(), mapSeriesArray, hashMap["annotations"].toString())
    }

    override fun getItemCount(): Int {
        return listRegisters.size
    }
}