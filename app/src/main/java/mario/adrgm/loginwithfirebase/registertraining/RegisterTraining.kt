package mario.adrgm.loginwithfirebase.registertraining

import android.os.Parcel
import android.os.Parcelable

data class RegisterTraining(
    var date: Long,
    var set: ArrayList<SetToRegister>,
    var annotations: String?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.createTypedArrayList(SetToRegister) as ArrayList<SetToRegister>,
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(date)
        parcel.writeString(annotations)
        parcel.writeTypedList(set)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RegisterTraining> {
        override fun createFromParcel(parcel: Parcel): RegisterTraining {
            return RegisterTraining(parcel)
        }

        override fun newArray(size: Int): Array<RegisterTraining?> {
            return arrayOfNulls(size)
        }
    }
}
