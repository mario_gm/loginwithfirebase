package mario.adrgm.loginwithfirebase.registertraining

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import mario.adrgm.loginwithfirebase.databinding.FragmentListRegisterBinding
import mario.adrgm.loginwithfirebase.exercises.ExercisesActivity
import mario.adrgm.loginwithfirebase.registertraining.adapters.AdapterTraining
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ListRegisterFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ListRegisterFragment : Fragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: AdapterTraining
    private var fragmentListRegisterFragmentBinding: FragmentListRegisterBinding? = null
    private val fragmentListBinding get() = fragmentListRegisterFragmentBinding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragmentListRegisterFragmentBinding =
            FragmentListRegisterBinding.inflate(inflater, container, false)
        return fragmentListBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = fragmentListBinding.recyclerRegisters
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        setListeners()
        fillData()
    }

    private fun returnValidSets(parameter: ArrayList<HashMap<*, *>>): ArrayList<RegisterTraining> {
        val register = ArrayList<RegisterTraining>()
        parameter.forEach { map ->
            register.add(
                RegisterTraining(
                    map["date"].toString().toLong(),
                    returnValidSet(map["set"] as ArrayList<HashMap<*, *>>)!!,
                    map["annotations"].toString()
                )
            )
        }
        return register
    }

    private fun returnValidSet(hashMap: ArrayList<HashMap<*, *>>): ArrayList<SetToRegister>? {
        val set = ArrayList<SetToRegister>()
        hashMap.forEach { map ->
            set.add(
                SetToRegister(
                    map["reps"].toString().toInt(),
                    map["reps2"].toString().toIntOrNull(),
                    map["pesoLbs"].toString().toFloat(),
                    map["pesoKg"].toString().toFloat(),
                    map["desc"].toString().toInt()
                )
            )
        }
        return set
    }

    private fun fillData() {
        val parameter = ExercisesActivity.exerciseInQuestion
        val correct: ArrayList<RegisterTraining>? = try {
            returnValidSets(parameter?.registerTraining as ArrayList<HashMap<*, *>>)
        }catch (e: Exception) {
            null
        }
        var listSorted: List<RegisterTraining>? = null
        try {
            listSorted = correct?.sortedByDescending { it.date }
        } catch (e: Exception) {
            Log.i("Tag", "Sin datos")
        }
        try {
            adapter = AdapterTraining(listSorted as MutableList<RegisterTraining>, parameter!!, (requireActivity() as RegisterActivity))
            recyclerView.adapter = adapter
        } catch (e: Exception) {
            Log.i("Tag", "Sin datos aun para: " + parameter?.name)
        }
    }

    private fun setListeners() {
        fragmentListBinding.buttonNewRegister.setOnClickListener(this)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ListRegisterFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ListRegisterFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            fragmentListBinding.buttonNewRegister.id -> {
                (requireActivity() as RegisterActivity).navigateToRegisterTraining(view, "")
            }
        }
    }
}