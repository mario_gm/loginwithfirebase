package mario.adrgm.loginwithfirebase.registertraining

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import mario.adrgm.loginwithfirebase.databinding.ActivityRegisterBinding
import mario.adrgm.loginwithfirebase.exercises.ExercisesActivity
import mario.adrgm.loginwithfirebase.registertraining.adapters.AdapterTraining
import mario.adrgm.loginwithfirebase.routines.SessionActivity

class RegisterActivity : AppCompatActivity() {
    private lateinit var activityBindingRegisterBinding: ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityBindingRegisterBinding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(activityBindingRegisterBinding.root)
    }

    override fun onBackPressed() {
        if (RegisterDataFragment.isActive) {
            AdapterTraining.clearRegister()
        }
        gotToExercisesForRetrieveData()
    }

    private fun gotToExercisesForRetrieveData() {
        val intentToRetrieveData = Intent(this, ExercisesActivity::class.java)
        intentToRetrieveData.putExtra(SessionActivity.RETRIEVE_DATA, true)
        startActivity(intentToRetrieveData)
    }

    fun navigateToRegisterTraining(view: View, exerciseId: String) {
        val action = ListRegisterFragmentDirections.actionListRegisterFragmentToRegisterDataFragment(exerciseId)
        view.findNavController().navigate(action)
    }

    fun showLoader() {
        activityBindingRegisterBinding.progressBar.visibility = View.VISIBLE
    }

    fun dismissLoader() {
        activityBindingRegisterBinding.progressBar.visibility = View.GONE
    }
}