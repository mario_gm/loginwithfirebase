package mario.adrgm.loginwithfirebase.registertraining

import android.os.Parcel
import android.os.Parcelable

data class SetToRegister(
    var reps: Int,
    var reps2: Int?,
    val pesoLbs: Float,
    var pesoKg: Float,
    val desc: Int
): Parcelable {
    constructor() : this(0, 0, 0f, 0f, 0)
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readFloat(),
        parcel.readFloat(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(reps)
        parcel.writeFloat(pesoKg)
        parcel.writeFloat(pesoLbs)
        parcel.writeInt(desc)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SetToRegister> {
        override fun createFromParcel(parcel: Parcel): SetToRegister {
            return SetToRegister(parcel)
        }

        override fun newArray(size: Int): Array<SetToRegister?> {
            return arrayOfNulls(size)
        }
    }
}
