package mario.adrgm.loginwithfirebase.registertraining

import android.app.AlertDialog
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.viewModels
import mario.adrgm.loginwithfirebase.R
import mario.adrgm.loginwithfirebase.databinding.FragmentRegisterDataBinding
import mario.adrgm.loginwithfirebase.exercises.ExercisesActivity
import mario.adrgm.loginwithfirebase.exercises.ExercisesModel
import mario.adrgm.loginwithfirebase.exercises.ExercisesViewModel
import mario.adrgm.loginwithfirebase.registertraining.adapters.AdapterTraining
import mario.adrgm.loginwithfirebase.utils.Status

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val REGISTER = "register"

/**
 * A simple [Fragment] subclass.
 * Use the [RegisterDataFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RegisterDataFragment : Fragment(), View.OnClickListener {
    private var listSaved: ArrayList<RegisterTraining>? = null
    private var registerDataFragmentBinding: FragmentRegisterDataBinding? = null
    private val registerBinding get() = registerDataFragmentBinding!!
    private var newRegisterTraining = ArrayList<RegisterTraining>()
    private var setToRegisterTraining = ArrayList<SetToRegister>()
    private var controlSet = 1
    private var exerciseId: String? = null
    private val exerciseViewModel: ExercisesViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            exerciseId = it.getString(REGISTER)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        registerDataFragmentBinding =
            FragmentRegisterDataBinding.inflate(inflater, container, false)
        return registerBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getList()
        subscribeLiveData()
        setListeners()
        setView()
    }

    private fun subscribeLiveData() {
        exerciseViewModel.liveExerciseUpdate.observe(viewLifecycleOwner) {
            when (it) {
                Status.SUCCESS -> {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.success_save),
                        Toast.LENGTH_LONG
                    ).show()
                    (requireActivity() as RegisterActivity).dismissLoader()
                    AdapterTraining.clearRegister()
                    requireActivity().onBackPressed()
                }
                Status.ERROR, null -> {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.not_valid_firebase_save),
                        Toast.LENGTH_LONG
                    ).show()
                    (requireActivity() as RegisterActivity).dismissLoader()
                }
                Status.LOADING -> {
                    (requireActivity() as RegisterActivity).showLoader()
                }
            }
        }
    }

    private fun getList() {
        ExercisesActivity.exerciseInQuestion?.registerTraining.also { listSaved = it }
        ExercisesActivity.exerciseInQuestion?.registerTraining = convertRegisterToCorrectForm()
        listSaved = ExercisesActivity.exerciseInQuestion?.registerTraining
    }

    private fun convertRegisterToCorrectForm(): ArrayList<RegisterTraining> {
        val listToReturn = ArrayList<RegisterTraining>()
        if (listSaved != null && !(listSaved!![0] is RegisterTraining)) {
            for (i in 0 until (listSaved?.size ?: 0)) {
                val map = listSaved!![i] as HashMap<*, *>
                val mapArray = map["set"] as ArrayList<*>
                val mapSeriesArray = ArrayList<SetToRegister>()
                for (j in 0 until mapArray.size) {
                    val set = mapArray[j] as HashMap<*, *>
                    val setToRegister = SetToRegister(
                        set["reps"].toString().toInt(),
                        set["reps2"].toString().toIntOrNull(),
                        set["pesoLbs"].toString().toFloat(),
                        set["pesoKg"].toString().toFloat(),
                        set["desc"].toString().toInt()
                    )
                    mapSeriesArray.add(
                        setToRegister
                    )
                }
                listToReturn.add(
                    RegisterTraining(
                        map["date"].toString().toLong(),
                        mapSeriesArray,
                        map["annotations"].toString()
                    )
                )
            }
        }
        return listToReturn
    }

    private fun enableOrDisabledPrevious(enabled: Boolean) {
        registerBinding.previousButton.isEnabled = enabled
        registerBinding.setNumber.text = controlSet.toString()
    }

    private fun setView() {
        registerBinding.tittleExercise.text = ExercisesActivity.exerciseInQuestion?.name
        enableOrDisabledPrevious(enabled = controlSet > 1)
        if (ExercisesActivity.exerciseInQuestion?.unilateral == true) {
            registerBinding.addReps2.visibility = View.VISIBLE
        }
        registerBinding.setNumber.text =
            Editable.Factory.getInstance().newEditable(controlSet.toString())
        fillData()
    }

    private fun fillData() {
        if (AdapterTraining.registerTraining != null) {
            registerBinding.addReps.text = Editable.Factory.getInstance()
                .newEditable(AdapterTraining.registerTraining!!.set[controlSet - 1].reps.toString())
            registerBinding.addReps2.text = Editable.Factory.getInstance()
                .newEditable(AdapterTraining.registerTraining!!.set[controlSet - 1].reps2.toString())
            registerBinding.addPeso.text = Editable.Factory.getInstance()
                .newEditable(AdapterTraining.registerTraining!!.set[controlSet - 1].pesoLbs.toString())
            registerBinding.addRest.text = Editable.Factory.getInstance()
                .newEditable(AdapterTraining.registerTraining!!.set[controlSet - 1].desc.toString())
        }
    }

    override fun onResume() {
        super.onResume()
        isActive = true
    }

    override fun onPause() {
        super.onPause()
        isActive = false
    }

    private fun setListeners() {
        registerBinding.finishButton.setOnClickListener(this)
        registerBinding.nextButton.setOnClickListener(this)
        registerBinding.previousButton.setOnClickListener(this)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment RegisterDataFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RegisterDataFragment().apply {
                arguments = Bundle().apply {
                    putString(REGISTER, exerciseId)
                }
            }

        var isActive = false
    }

    private fun validateInputs(): Boolean {
        return registerBinding.addPeso.text.isNotEmpty() && registerBinding.addReps.text.isNotEmpty() &&
                registerBinding.addRest.text.isNotEmpty()
    }

    private fun clearInputs() {
        registerBinding.addPeso.text = Editable.Factory.getInstance().newEditable("")
        registerBinding.addReps.text = Editable.Factory.getInstance().newEditable("")
        registerBinding.addReps2.text = Editable.Factory.getInstance().newEditable("")
        registerBinding.addRest.text = Editable.Factory.getInstance().newEditable("")
    }

    private fun showToast() {
        Toast.makeText(requireContext(), getString(R.string.not_valid_data), Toast.LENGTH_SHORT)
            .show()
    }

    private fun addToDataBase(exercise: ExercisesModel) {
        exerciseViewModel.updateExercise(exercise)
    }

    private fun convertLbsToKg(lbs: Float): Float {
        return lbs * 0.45359237f
    }

    private fun createSet(): SetToRegister {
        val pesoLbs = registerBinding.addPeso.text.toString().toFloat()
        return SetToRegister(
            registerBinding.addReps.text.toString().toInt(),
            registerBinding.addReps2.text.toString().toIntOrNull(),
            pesoLbs,
            convertLbsToKg(pesoLbs),
            registerBinding.addRest.text.toString().toInt()
        )
    }

    private fun showLast() {
        registerBinding.addReps.text =
            Editable.Factory.getInstance().newEditable(
                setToRegisterTraining[setToRegisterTraining.size - 1].reps
                    .toString()
            )
        registerBinding.addRest.text =
            Editable.Factory.getInstance().newEditable(
                setToRegisterTraining[setToRegisterTraining.size - 1].desc
                    .toString()
            )
        registerBinding.addPeso.text =
            Editable.Factory.getInstance().newEditable(
                setToRegisterTraining[setToRegisterTraining.size - 1].pesoLbs
                    .toString()
            )
        if (ExercisesActivity.exerciseInQuestion?.unilateral == true) {
            registerBinding.addReps2.text =
                Editable.Factory.getInstance().newEditable(
                    setToRegisterTraining[setToRegisterTraining.size - 1].reps2
                        .toString()
                )
        }
    }

    private fun addAnnotations() {
        val builderAlert: AlertDialog.Builder = AlertDialog.Builder(requireContext())
        builderAlert.setMessage(requireContext().getString(R.string.add_annotations_text))
        builderAlert.setCancelable(false)

        // Set up the input
        val input = EditText(requireContext())
        input.inputType = InputType.TYPE_CLASS_TEXT
        if (AdapterTraining.registerTraining != null) {
            input.text = Editable.Factory.getInstance().newEditable(AdapterTraining.registerTraining!!.annotations.toString())
        }
        builderAlert.setView(input)

        builderAlert.setPositiveButton(
            "Cancelar"
        ) { dialog, _ ->
            dialog.cancel()
        }

        builderAlert.setNegativeButton(
            "Continuar"
        ) { dialog, _ ->
            val annotations = if (input.text.toString().isNotEmpty()) {
                input.text.toString()
            } else {
                null
            }

            newRegisterTraining
                .add(
                    RegisterTraining(
                        if (AdapterTraining.registerTraining == null) {System.currentTimeMillis()}
                        else {AdapterTraining.registerTraining!!.date},
                        setToRegisterTraining,
                        annotations
                    )
                )
            updateRegisterTraining(ExercisesActivity.exerciseInQuestion?.registerTraining)
            addToDataBase(ExercisesActivity.exerciseInQuestion!!)
            dialog.cancel()
        }

        val alertShow: AlertDialog = builderAlert.create()
        alertShow.show()
    }

    private fun updateRegisterTraining(registerTraining: ArrayList<RegisterTraining>?) {
        ExercisesActivity.exerciseInQuestion?.registerTraining =
            when {
                registerTraining.isNullOrEmpty() -> {
                    newRegisterTraining
                }
                AdapterTraining.registerTraining == null -> {
                    (ExercisesActivity.exerciseInQuestion?.registerTraining?.plus(
                        newRegisterTraining
                    )) as ArrayList<RegisterTraining>
                }
                else -> {
                    var edit: RegisterTraining? = null
                    newRegisterTraining.forEach {
                        if (it.date == AdapterTraining.registerTraining?.date)
                            edit = it
                    }
                    listSaved?.forEach {
                        if (it.date == edit?.date) {
                            for (i in 0 until  it.set.size) {
                                try {
                                    it.set[i] = edit!!.set[i]
                                }catch (e: Exception) {
                                    Log.d("Tag", "Task completed")
                                    break
                                } finally {
                                    it.annotations = edit!!.annotations
                                }
                            }
                            edit = it
                        }
                    }
                    registerTraining
                }
            }
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            registerBinding.nextButton.id -> {
                if (validateInputs()) {
                    controlSet++
                    setToRegisterTraining.add(createSet())
                    enableOrDisabledPrevious(enabled = controlSet > 1)
                    if (AdapterTraining.registerTraining != null &&
                        controlSet <= AdapterTraining.registerTraining!!.set.size)
                        fillData()
                    else
                        clearInputs()
                } else {
                    showToast()
                }
            }
            registerBinding.previousButton.id -> {
                controlSet--
                showLast()
                setToRegisterTraining.removeLast()
                enableOrDisabledPrevious(enabled = controlSet > 1)
            }
            registerBinding.finishButton.id -> {
                if (validateInputs()) {
                    setToRegisterTraining.add(createSet())
                    addAnnotations()
                } else {
                    showToast()
                }
            }
        }
    }
}