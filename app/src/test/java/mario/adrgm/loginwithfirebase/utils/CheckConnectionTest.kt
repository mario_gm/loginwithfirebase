package mario.adrgm.loginwithfirebase.utils

import io.mockk.*
import mario.adrgm.loginwithfirebase.login.LoginViewModel
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.net.InetAddress

class CheckConnectionTest {

    private lateinit var loginViewModel: LoginViewModel

    @Before
    fun setUp() {
        loginViewModel = mockk(relaxed = true)
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

    @Test
    fun `isInternetAvailable sets connection variable to true when internet is available`() {
        // Given
        mockkStatic(InetAddress::class)
        every { InetAddress.getByName("google.com") } returns mockk(relaxed = true)

        // When
        CheckConnection().isInternetAvailable(loginViewModel)

        // Then
        verify { loginViewModel.setConnectionVariable(true) }
    }

    @Test
    fun `isInternetAvailable sets connection variable to false when internet is not available`() {
        // Given
        mockkStatic(InetAddress::class)
        every { InetAddress.getByName("google.com") } throws Exception()

        // When
        CheckConnection().isInternetAvailable(loginViewModel)

        // Then
        verify { loginViewModel.setConnectionVariable(false) }
    }
}